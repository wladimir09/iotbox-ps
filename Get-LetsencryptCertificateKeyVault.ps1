#######################################################################################
# 
# Script that gets Let's Encrypt certificate and uploads it to Azure Key Vault 
# (that are used on Application gateway)
# Pre-requirements:
#      - Have a storage account in which the folder path has been created: 
#        '/.well-known/acme-challenge/', to put here the Let's Encrypt DNS check files
 
#      - Add "Path-based" rule in the Application Gateway with this configuration: 
#           - Path: '/.well-known/acme-challenge/*'
#           - Check the configure redirection option
#           - Choose redirection type: permanent
#           - Choose redirection target: External site
#           - Target URL: <Blob public path of the previously created storage account>
#                - Example: 'https://test.blob.core.windows.net/public'
#
#
#        Following modules are needed now: Az.Accounts, Az.Network, Az.Storage, Az.KeyVault, ACME-PS
#
#######################################################################################
 
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# [CmdletBinding()]
# param(
# # Domain to get the certificate for
#     [parameter(Mandatory=$true)]
#     [string]$Domain,
#     [parameter(Mandatory=$true)]
#     [string]$EmailAddress,
# # Resource group name where blob starage resides
#     [parameter(Mandatory=$true)]
#     [string]$STResourceGroupName,
# # Blob storage name
#     [parameter(Mandatory=$true)]
#     [string]$storageName,
#     [parameter(Mandatory=$true)]
#     [string]$KeyVaultName,
#     [parameter(Mandatory=$true)]
#     [string]$CertNameinKeyVault
# )

# Ensures that no login info is saved after the runbook is done
Disable-AzContextAutosave

#$PSModuleAutoloadingPreference = 'None'

#Import-Module -Name Az.Accounts, Az.Network, Az.Storage, Az.KeyVault, ACME-PS

# Log in azure
try
{
    "Logging in to Azure..."
    Connect-AzAccount -Identity
}
catch {
    Write-Error -Message $_.Exception
    throw $_.Exception
}
        $Domain='iotbox.eastus2.cloudapp.azure.com'
        $EmailAddress='innovacion@celsia.com'
        $STResourceGroupName='iotbox-rg'
        $storageName='iotboxhsprd'
        $KeyVaultName='iotbox-kev-prd'
        $CertNameinKeyVault='certiotboxprd'
      
        Write-Output "Trying to get a new certificate for $domain"
 
        # Create a state object and save it to the harddrive
        $tempFolderPath = $env:TEMP + "\" + $domain
        
        # Preparing folder for certificate renewal
        # Remove folder used for certificate renewal if existing
        
        Write-Output "Preparing folder for certificate renewal"
        
        if(Test-Path $tempFolderPath -PathType Container)
        {            
            Write-Output "Remove folder used for certificate renewal if existing"
            Get-ChildItem -Path $tempFolderPath -Recurse | Remove-Item -force -recurse
            Remove-Item $tempFolderPath -Force
        }        
        
        $tempFolder = New-Item -Path $tempFolderPath -ItemType "directory"
        $state = New-ACMEState -Path $tempFolder
        $serviceName = 'LetsEncrypt'
 
        # Fetch the service directory and save it in the state
        Write-Output "Fetch the service directory and save it in the state"
        Get-ACMEServiceDirectory $state -ServiceName $serviceName -PassThru;
 
        # Get the first anti-replay nonce
        Write-Output "Get the first anti-replay nonce"
        New-ACMENonce $state;
 
        # Create an account key. The state will make sure it's stored.
        Write-Output "Create an account key. The state will make sure it's stored."
        New-ACMEAccountKey $state -PassThru;
 
        # Register the account key with the acme service. The account key will automatically be read from the state
        Write-Output "Register the account key with the acme service. The account key will automatically be read from the state."
        New-ACMEAccount $state -EmailAddresses $EmailAddress -AcceptTOS;
 
        # Load an state object to have service directory and account keys available
        Write-Output "Load an state object to have service directory and account keys available"
        $state = Get-ACMEState -Path $tempFolder;
 
        # It might be neccessary to acquire a new nonce, so we'll just do it for the sake of the example.
        Write-Output "It might be neccessary to acquire a new nonce, so we'll just do it for the sake of the example"
        New-ACMENonce $state -PassThru;
 
        # Create the identifier for the DNS name
        Write-Output "Create the identifier for the DNS name"
        $identifier = New-ACMEIdentifier $domain;
 
        # Create the order object at the ACME service.
        Write-Output "Create the order object at the ACME service."
        $order = New-ACMEOrder $state -Identifiers $identifier;
 
        # Fetch the authorizations for that order
        Write-Output "Fetch the authorizations for that order"
        $authZ = Get-ACMEAuthorization -State $state -Order $order;
 
        # Select a challenge to fullfill
        Write-Output "Select a challenge to fullfill"
        $challenge = Get-ACMEChallenge $state $authZ "http-01";
 
        # Inspect the challenge data
        Write-Output "Inspect the challenge data"
        $challenge.Data;
 
        # Create the file requested by the challenge
        Write-Output "Create the file requested by the challenge"
        $fileName = $tempFolderPath + '\' + $challenge.Token;
        Write-Output "fileName $fileName"
        Set-Content -Path $fileName -Value $challenge.Data.Content -NoNewline;
        Write-Output "Set-Content ready"

        $blobName = ".well-known/acme-challenge/" + $challenge.Token
        Write-Output "blobName $blobName"

        #$storageAccount = Get-AzStorageAccount -ResourceGroupName $STResourceGroupName -Name $StorageName
        #Write-Output "storageAccount $storageAccount"
        #$ctx = $storageAccount.Context

        $STORAGE_ACCOUNT_KEY = (Get-AzStorageAccountKey -ResourceGroupName $STResourceGroupName -Name $StorageName).Value[0]
        $STORAGE_ACCOUNT_CS = "DefaultEndpointsProtocol=https;AccountName=$StorageName;AccountKey=$STORAGE_ACCOUNT_KEY;EndpointSuffix=core.windows.net"

        $ctx = New-AzStorageContext -ConnectionString $STORAGE_ACCOUNT_CS
        Write-Output "ctx $ctx"
        Set-AzStorageBlobContent -File $fileName -Container "public" -Context $ctx -Blob $blobName
        Write-Output "AzStorageBlobContent ready"

        # Signal the ACME server that the challenge is ready
        Write-Output "Signal the ACME server that the challenge is ready"
        $challenge | Complete-ACMEChallenge $state;

        # Wait a little bit and update the order, until we see the states
        Write-Output "Wait a little bit and update the order, until we see the states"
        while ($order.Status -notin ("ready", "invalid")) {
            Start-Sleep -Seconds 10;
            $order | Update-ACMEOrder $state -PassThru;
        }

        # Should the order get invalid, use Get-ACMEAuthorizationError to list error details.
        if($order.Status -ieq ("invalid")) {
            $order | Get-ACMEAuthorizationError $state;
            throw "Order was invalid";
        }


        # We should have a valid order now and should be able to complete it
        # Therefore we need a certificate key
        Write-Output "We should have a valid order now and should be able to complete it
        Therefore we need a certificate key"
        $certKey = New-ACMECertificateKey -Path "$tempFolder\$domain.key.xml";
 
        # Complete the order - this will issue a certificate singing request
        Write-Output "Complete the order - this will issue a certificate singing request"
        Complete-ACMEOrder $state -Order $order -CertificateKey $certKey;
 
        # Now we wait until the ACME service provides the certificate url
        Write-Output "Now we wait until the ACME service provides the certificate url"
        while (-not $order.CertificateUrl) {
            Start-Sleep -Seconds 15
            $order | Update-Order $state -PassThru
        }
 
        # As soon as the url shows up we can create the PFX
        Write-Output "As soon as the url shows up we can create the PFX"
        $password = ConvertTo-SecureString -String "GOOD_LONG_SECRET" -Force -AsPlainText
        Export-ACMECertificate $state -Order $order -CertificateKey $certKey -Path "$tempFolder\$domain.pfx" -Password $password;
 
        # Delete blob to check DNS
        Write-Output "Delete blob to check DNS"
        Remove-AzStorageBlob -Container "public" -Context $ctx -Blob $blobName
 
        ### Upload new Certificate version to KeyVault
        Write-Output "Upload new Certificate version to KeyVault"
        Import-AzKeyVaultCertificate -VaultName $KeyVaultName -Name $CertNameinKeyVault -FilePath "$tempFolder\$domain.pfx" -Password $password
 
 
Write-Output "Completed. Check the certificates in Key Vault. there should be the new one named $CertNameinKeyVault"